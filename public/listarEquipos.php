<?php
require "../src/Equipo.php";
  $e=new Equipo();
  $e->conectar();
  $res3=$e->listaEquipos();
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Lista de jugadores</title>
    <link rel="stylesheet" href="./css/nba.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css">
  </head>
  <body>
      <?php include "./assets/navSup.php"; ?>
    <table align=center border=1px>
          <td style="text-align:center;padding:25px">Nombre</td>
          <td style="text-align:center;padding:25px">Ciudad</td>
          <td style="text-align:center;padding:25px">Conferencia</td>
          <td style="text-align:center;padding:25px">Division</td>
      </tr>
      <?php foreach ($res3 as $usuario) {
        echo "<tr>";
        echo "<td style='text-align:center'>".$usuario["Nombre"]."</td>";
        echo "<td style='text-align:center'>".$usuario["Ciudad"]."</td>";
        echo "<td style='text-align:center'>".$usuario["Conferencia"]."</td>";
        echo "<td style='text-align:center'>".$usuario["Division"]."</td>";
      }
      ?>
    </tr>
  </table>
</body>
</html>
