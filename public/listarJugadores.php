<?php
require "../src/Jugador.php";
  $j=new Jugador();
  $j->conectar();
  $res2=$j->listaJugadores();
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Lista de jugadores</title>
    <link rel="stylesheet" href="./css/nba.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css">
  </head>
  <body>
      <?php include "./assets/navSup.php"; ?>
    <table align=center border=1px>
          <td style="text-align:center;padding:25px">ID</td>
          <td style="text-align:center;padding:25px">Nombre</td>
          <td style="text-align:center;padding:25px">Peso</td>
      </tr>
      <?php foreach ($res2 as $usuario) {
        echo "<tr>";
        echo "<td style='text-align:center'>".$usuario["codigo"]."</td>";
        echo "<td style='text-align:center'>".$usuario["Nombre"]."</td>";
        echo "<td style='text-align:center'>".$usuario["Peso"]." lbs.</td>";
      }
      ?>
    </tr>
  </table>
</body>
</html>
